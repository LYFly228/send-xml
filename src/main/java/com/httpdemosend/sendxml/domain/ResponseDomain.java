package com.httpdemosend.sendxml.domain;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Cirilla
 * @projectName send-xml
 * @description:
 * @date 2020/9/21  17:27
 */
@XmlRootElement(name ="response")
@Data
public class ResponseDomain {

    private Long id;
    private String name;
    private Integer code;
    private String content;
}
