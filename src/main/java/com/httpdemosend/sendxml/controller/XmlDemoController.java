package com.httpdemosend.sendxml.controller;

import cn.hutool.core.util.XmlUtil;
import com.httpdemosend.sendxml.domain.ResponseDomain;
import com.httpdemosend.sendxml.util.TripleDESUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author Cirilla
 * @projectName send-xml
 * @description:
 * @date 2020/9/21  17:31
 */
@RestController
public class XmlDemoController {

    @PostMapping(value ="/post")
    public void getXmlDemo(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        byte[] bytes = file.getBytes();
//        System.out.println(bytes.length);
        ServletInputStream inputStream = request.getInputStream();
        GZIPInputStream gzis = new GZIPInputStream(inputStream);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int len = 0;
        while ((len = gzis.read(buf)) != -1) {
            baos.write(buf, 0, len);
        }
        gzis.close();
        byte[] data = baos.toByteArray();
        baos.close();
        String requestStr = new String(data);
        String decode = TripleDESUtils.decode(requestStr);
        System.out.println("请求xml: " + decode);

        ResponseDomain xmlDemo = new ResponseDomain();
        xmlDemo.setId(1L);
        xmlDemo.setName("响应方");
        xmlDemo.setCode(200);
        xmlDemo.setContent("response 返回xml");
        Document document = XmlUtil.beanToXml(xmlDemo);
        String responseXml = XmlUtil.format(document);
        String encode = TripleDESUtils.encode(responseXml);
        System.out.println("加密后的xml：" + encode);
        ServletOutputStream outputStream = response.getOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(outputStream);
//        ZipEntry entry = new ZipEntry("test");
//        zos.putNextEntry(entry);
        zos.write(encode.getBytes());
        zos.finish();
        zos.close();

    }
}
