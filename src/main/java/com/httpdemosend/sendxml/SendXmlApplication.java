package com.httpdemosend.sendxml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class SendXmlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SendXmlApplication.class, args);
    }

}
